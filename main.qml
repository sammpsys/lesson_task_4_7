import QtQuick 2.15
import QtQuick.Window 2.15

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Material 2.3

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Welcome to the quiz")
    color: "#16FF16"



        Label {
            width: parent.width
            wrapMode: Label.Wrap
            anchors.left: parent.left
            horizontalAlignment: Qt.AlignLeft
            text: "Question 1/8   User1   Points: 0 "
            font.family: "Comic Sans MS";
            }
        Rectangle {
            width: parent.width/4
            anchors.right: parent.right
            Label {
                anchors.top: parent.top
                anchors.right: parent.right
                wrapMode: Label.Wrap
                text: "User 1: 0 pts \n User 2: 0 pts \n User 3: 0 pts \n User 4: 0 pts"
                font.family: "Comic Sans MS";
                horizontalAlignment: Qt.AlignRight
            }
        }


        GridLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.top: parent.verticalCenter
            columns: 4
            rows: 6

            Rectangle {
                Layout.column: 0
                Layout.columnSpan: 4
                Layout.row: 0
                Layout.rowSpan: 1

                Layout.fillHeight: true
                Layout.fillWidth: true
                anchors.top:parent.top
                color: "burlywood"
                Label{
                            width: parent.width
                            wrapMode: Label.Wrap
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            horizontalAlignment: Qt.AlignHCenter
                            verticalAlignment:Qt.AlignVCenter
                            text: "LeBestQuiz"
                            font.family: "Comic Sans MS";

                            }
            }

            Rectangle {
                Layout.column: 0
                Layout.columnSpan: 4
                Layout.row: 1
                Layout.rowSpan: 1

                Layout.fillHeight: true
                Layout.fillWidth: true
                //anchors.top:parent.top
                color: "steelblue"


                Label{
                            width: parent.width
                            wrapMode: Label.Wrap
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            horizontalAlignment: Qt.AlignHCenter
                            verticalAlignment:Qt.AlignVCenter
                            text: "Question 1: What is the colour of the sky?"
                            font.family: "Comic Sans MS";

                            }
            }

            Rectangle {
                Layout.column: 0
                Layout.columnSpan: 2
                Layout.row: 2
                Layout.rowSpan: 2

                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "#16FF16"

                Button {
                    text: "Yellow"
                    font.family: "Comic Sans MS";
                    Material.background:Material.Yellow
                    Layout.fillWidth: true
                    anchors.fill: parent
                    anchors.leftMargin: 5
                    }
                }
            Rectangle {
                Layout.column: 0
                Layout.columnSpan: 2
                Layout.row: 4
                Layout.rowSpan: 2


                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "#16FF16"

                Button {
                    text: "Pink"
                    font.family: "Comic Sans MS";
                    Material.background:Material.Pink
                    Layout.fillWidth: true
                    anchors.fill: parent
                    anchors.leftMargin: 5
                    }
                }
            Rectangle {
                Layout.column: 2
                Layout.columnSpan: 2
                Layout.row: 2
                Layout.rowSpan: 2


                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "#16FF16"

                Button {
                    text: "Red"
                    font.family: "Comic Sans MS";
                    Material.background:Material.Red
                    Layout.fillWidth: true
                    anchors.fill: parent
                    anchors.rightMargin: 5
                    }
                }
            Rectangle {
                Layout.column: 2
                Layout.columnSpan: 2
                Layout.row: 4
                Layout.rowSpan: 2


                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "#16FF16"

                Button {
                    text: "Blue"
                    font.family: "Comic Sans MS";
                    Material.background:Material.Blue
                    Layout.fillWidth: true
                    anchors.fill: parent
                    anchors.rightMargin: 5
                    }
                }



        }






}

