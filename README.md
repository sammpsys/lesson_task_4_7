# Task 4.7
The repo contains the quiz game in QML form. 

## Requirements

Requires `QTCreator`.

## Setup
    ```sh
    1. Clone the repo 
    2. Open, Build and Run in Qt Creator
    ```

## Maintainers

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

## Contributors and License

Copyright 2022,

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

